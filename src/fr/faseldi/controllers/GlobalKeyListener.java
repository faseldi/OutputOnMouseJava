package fr.faseldi.controllers;

import fr.faseldi.outputonmousejava.FactorySingleton;
import java.util.ArrayList;
import java.util.List;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
/**
 *
 * @author faseldi
 */
public class GlobalKeyListener implements NativeKeyListener{
    private List<Integer> keyPressed;
    private static GlobalKeyListener instance;
    public static final GlobalKeyListener getInstance(){
        if(instance == null){
            instance = new GlobalKeyListener();
        }
        return instance;
    }
    private GlobalKeyListener(){
        keyPressed = new ArrayList<>();
    }
    /**
     * used to detect the use of shortcuts
     * @param e 
     */
    @Override
    public void nativeKeyPressed(NativeKeyEvent e){
        keyPressed.add(e.getKeyCode());
        if(e.getKeyCode() != NativeKeyEvent.VC_S &&
           e.getKeyCode() != NativeKeyEvent.VC_E &&
           e.getKeyCode() != NativeKeyEvent.VC_SHIFT_L && 
           e.getKeyCode() != NativeKeyEvent.VC_CONTROL_L){
            keyPressed.clear();
        }
        if(keyPressed.size() >= 2){
            if(keyPressed.contains(NativeKeyEvent.VC_CONTROL_L) && keyPressed.contains(NativeKeyEvent.VC_S)){
                // action on save
                new Thread( () -> {
                    FactorySingleton.getOutputVue().setVisible(true);
                }).start();
                keyPressed.clear();
            }
            if(keyPressed.contains(NativeKeyEvent.VC_CONTROL_L) && keyPressed.contains(NativeKeyEvent.VC_SHIFT_L)
                    && keyPressed.contains(NativeKeyEvent.VC_E)){
                // action on edit
                FactorySingleton.getPreferenceVue().setVisible(true);
                keyPressed.clear();
            }
        }
    }
    @Override
    public void nativeKeyTyped(NativeKeyEvent e){
    }
    @Override
    public void nativeKeyReleased(NativeKeyEvent e){
        keyPressed.remove((Integer)e.getKeyCode()); // cast pour pas qu'il utilise la methode remove indice
    }
}
