package fr.faseldi.controllers;

import fr.faseldi.outputonmousejava.FactorySingleton;
import fr.faseldi.vues.OutputVue;
import java.awt.Point;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseMotionListener;

/**
 *
 * @author faseldi
 */
public class GlobalMouseListener implements NativeMouseMotionListener{
    private int posX, posY;
    private static GlobalMouseListener instance;
    public static final GlobalMouseListener getInstance(){
        if(instance == null){
            instance = new GlobalMouseListener();
        }
        return instance;
    }
    private GlobalMouseListener(){
        this.posX = 0;
        this.posY = 0;
    }
    public int getX(){
        return posX;
    }
    public int getY(){
        return posY;
    }
    /**
     * used to know the mouse's position and to close or not the frame
     * @param nme 
     */
    @Override
    public void nativeMouseMoved(NativeMouseEvent nme) {
        this.posX = nme.getX();
        this.posY = nme.getY();
        if(FactorySingleton.getOutputVue().isVisible()){
            new Thread( () -> {
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){}
                OutputVue outputVue = FactorySingleton.getOutputVue();
                Point location = outputVue.getLocation();
                while(posX >= location.x && posX <= location.x + outputVue.getWidth()
                   && posY >= location.y && posY <= location.y + outputVue.getHeight()){
                    try{
                        Thread.sleep(100);
                    }catch(InterruptedException e){break;}
                }
                FactorySingleton.getOutputVue().setVisible(false);
            }).start();
        }
    }

    @Override
    public void nativeMouseDragged(NativeMouseEvent nme) {
    }
}
