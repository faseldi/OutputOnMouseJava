package fr.faseldi.controllers;

import fr.faseldi.vues.PreferencesVue;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 *
 * @author faseldi
 */
public class ProjectRunController {
    private static final long TIMEOUT = 3000;
    private static ProjectRunController instance;
    public static final ProjectRunController getInstance(){
        if(instance == null){
            instance = new ProjectRunController();
        }
        return instance;
    }
    private ProjectRunController(){}
    /**
     * uses the javac or ant output to get the result
     * @return the compilation output
     */
    public String getOutput(){
        try{
            String path = PreferencesVue.getMainPath();
            String builder;
            try{
                builder = PreferencesVue.getBuilder();
            }catch(Exception e){
                builder = "Javac";
            }
            Process run;
            Process ifEchec = null;
            switch(builder){
                case "Ant": 
                    run = Runtime.getRuntime().exec("ant -f "+path+" run");
                    break;
                default :
                    File compiled = new File(path);
                    // compile
                    try{
                        Runtime.getRuntime()
                                .exec("rm "+compiled.getParent()+File.separator+"*.class")
                                .waitFor();
                        ifEchec = Runtime.getRuntime().exec("javac "+path);
                        ifEchec.waitFor();
                    }catch(InterruptedException e){
                    }
                    run = Runtime.getRuntime().exec("java -cp "
                            +compiled.getParent()
                            +" "+compiled.getName().replace(".java", ""));
                    break;
            }
            try{
                new Thread( () -> {
                    try{
                        Thread.sleep(TIMEOUT);
                        run.destroyForcibly();
                    }catch(InterruptedException e){}
                }).start();
                run.waitFor(TIMEOUT, TimeUnit.MILLISECONDS);
            }catch(InterruptedException e){
                run.destroyForcibly();
                return "Timeout";
            }
            switch(builder){
                case "Ant":
                    return normalStreamAnt(run);
                default:
                    return normalStreamJavac(run, ifEchec);
            }

        }catch(IOException e){
            e.printStackTrace();
            return "Error, unable to find the project";
        }
    }
    private String normalStreamJavac(Process run, Process ifechec) throws IOException {
        String res = "";
        String line;
        BufferedReader buff = new BufferedReader(
                new InputStreamReader(run.getInputStream())
        );
        while( (line = buff.readLine()) != null ){
            res += line+"\n";
        }
        if(res.isEmpty()){
            buff = new BufferedReader(
                    new InputStreamReader(ifechec.getErrorStream())
            );
            while ( (line = buff.readLine()) != null){
                res += line+"\n";
            }
        }
        if(res.isEmpty()){
            buff = new BufferedReader(
                    new InputStreamReader(run.getErrorStream())
            );
            while( (line = buff.readLine()) != null){
                res += line+"\n";
            }
        }
        return res;
    }
    private String normalStreamAnt(Process run) throws IOException{
        final List<String> arr = new ArrayList<>();
        String line;
        BufferedReader buff = new BufferedReader(
                new InputStreamReader(run.getInputStream())
        );
        while( (line = buff.readLine()) != null ){
            Arrays.asList(line.split("\n"))
                  .stream()
                  .filter( s -> s.contains("[java]") || s.contains("[javac]"))
                  .forEach( s -> {
                      String[] spl = s.split("\\[java\\] ");
                      if(spl.length >= 2){
                          arr.add(spl[spl.length-1]);
                      }
                      spl = s.split("\\[javac\\]");
                      if(spl.length >= 2){
                          arr.add(spl[spl.length-1]);
                      }
                  });
        }
        run.destroy();
        String res = "";
        for(String s : arr){
            res += s+"\n";
        }
        return res;
    }
}
