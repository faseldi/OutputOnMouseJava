package fr.faseldi.vues;

import fr.faseldi.outputonmousejava.FactorySingleton;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

/**
 * view to display the compiler output
 * @author faseldi
 */
public class OutputVue extends JFrame{
    private JTextPane outputPane;
    private static int FONT_HEIGHT;//px
    private Color font_color;
    private Color background_color;
    private static OutputVue instance;
    public static final OutputVue getInstance(){
        if(instance == null){
            instance = new OutputVue();
        }
        return instance;
    }
    private OutputVue(){
        super();
        outputPane = new JTextPane();
        outputPane.setEditable(false);
        outputPane.setFont( new Font(outputPane.getFont().getName(), Font.PLAIN, 14));
        FONT_HEIGHT = getFontMetrics(outputPane.getFont()).getHeight();
        add(outputPane);
        init();
        setUndecorated(true);
    }
    private void init(){
        font_color = new Color(243,243,243);
        background_color = new Color(97,97,97);
        
        outputPane.setForeground(font_color);
        outputPane.setBackground(background_color);
    }
    @Override
    public void setVisible(boolean visible){
        if(visible){
            String text;
            String builder = "Javac";
            try{
                builder = PreferencesVue.getBuilder();
            }catch(DOMException | IOException | ParserConfigurationException | SAXException e){}
            if(!PreferencesVue.getMainPath().isEmpty() && 
                    (builder.equals("Ant") | builder.equals("Javac"))
            ){
                text = FactorySingleton.getProjectRun().getOutput();
            }else{
                text = "Builder unsupported\n Please manage your project using CTRL + SHIFT + E";
            }
            outputPane.setText(text);
            String[] spl = text.split("\n");
            setSize(outputPane.getSize());
            FontMetrics metrics = outputPane.getFontMetrics(outputPane.getFont());
            int max = metrics.stringWidth(
                    Arrays.asList(text.split("\n"))
                    .stream()
                    .max( Comparator.comparing( s -> metrics.stringWidth(s) ))
                    .get()
                );
            setSize( max +10, spl.length * FONT_HEIGHT +2);
            setLocation(FactorySingleton.getMouseListener().getX(), FactorySingleton.getMouseListener().getY());
        }
        super.setVisible(visible);
    }
}
