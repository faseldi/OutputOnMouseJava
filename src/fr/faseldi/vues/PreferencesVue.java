package fr.faseldi.vues;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;

/**
 *
 * @author faseldi
 */
public class PreferencesVue extends JFrame {
    private String mainPathComplete;
    private static PreferencesVue instance = new PreferencesVue();
    public static final PreferencesVue getInstance(){
        return instance;
    }
    /**
     * Creates new form PreferencesVue
     */
    public PreferencesVue() {
        initComponents();
        try{
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        }catch(Exception e){}
        setResizable(false);
        try{
            initContent();
        }catch(Exception e){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                    "Unable to read or locate the prefrences file");            
        }
    }
    public static final String getMainPath(){
        try{
            File mainPath = new File(getNodes().getNamedItem("mainPath")
            .getNodeValue()
            .replace(":", "/"));
            return mainPath.getAbsolutePath();
        }catch(Exception e){
            return "";
        }
    }
    public static final String getBuilder() 
            throws DOMException, IOException, SAXException, ParserConfigurationException{
        String builder = getNodes().getNamedItem("builder").getNodeValue();
        return builder;
    }
    private static NamedNodeMap getNodes() 
            throws SAXException, IOException, ParserConfigurationException{
        File pref = new File("./preferences.xml");
        DocumentBuilder build = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = build.parse(pref);
        return doc.getChildNodes().item(0).getAttributes();
    }
    @Override
    public void setVisible(boolean visible){
        try{
            initContent();
        }catch(Exception e){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                    "Unable to read or locate the prefrences file");
        }
        super.setVisible(visible);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    public void init(){
        cancelButton.addActionListener( event -> {
            this.dispose();
        });
        applyButton.addActionListener( event -> {
            try{
                save();
            }catch(ParserConfigurationException | TransformerException e){
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, 
                        "Error while save preferences : \n");
            }
            this.dispose();
        });
        mainPathButton.addActionListener( event -> {
            JFileChooser fileChooser = new JFileChooser();
            int result = fileChooser.showOpenDialog(this);
            if(result == JFileChooser.APPROVE_OPTION){
                mainPathComplete = fileChooser.getSelectedFile().getPath()
                        .replace("/", ":");
                mainPath.setText(fileChooser.getSelectedFile().getName());
            }
        });
    }
    private void save() 
            throws ParserConfigurationException, TransformerConfigurationException, TransformerException{
        DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
        DocumentBuilder docB = docF.newDocumentBuilder();
        Document doc = docB.newDocument();
        Element root = doc.createElement("Preferences");
        doc.appendChild(root);
        root.setAttribute("builder", this.builder.getSelectedItem().toString());
        root.setAttribute("mainPath", this.mainPathComplete);

        // write
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("./preferences.xml"));
        transformer.transform(source, result);
    }
    private void initContent() 
            throws FileNotFoundException, SAXException, IOException, ParserConfigurationException{
        this.builder.setSelectedItem(getBuilder());
        this.mainPathComplete = getMainPath();
        this.mainPath.setText(new File(mainPathComplete).getName());
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        applyButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        builder = new javax.swing.JComboBox<>();
        projectbuildLabel = new javax.swing.JLabel();
        mainPath = new javax.swing.JLabel();
        mainPathButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        applyButton.setText("Apply");

        cancelButton.setText("Cancel");
        cancelButton.setToolTipText("");

        builder.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ant", "Javac" }));

        projectbuildLabel.setText("Project Builder");

        mainPathButton.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        mainPathButton.setText("Main path");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(projectbuildLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(builder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(cancelButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(applyButton))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 6, Short.MAX_VALUE)
                        .addComponent(mainPathButton, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mainPath, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(builder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(projectbuildLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mainPathButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mainPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 197, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(applyButton)
                    .addComponent(cancelButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyButton;
    private javax.swing.JComboBox<String> builder;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel mainPath;
    private javax.swing.JButton mainPathButton;
    private javax.swing.JLabel projectbuildLabel;
    // End of variables declaration//GEN-END:variables
}
