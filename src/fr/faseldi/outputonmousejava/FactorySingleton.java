package fr.faseldi.outputonmousejava;

import fr.faseldi.controllers.GlobalKeyListener;
import fr.faseldi.controllers.GlobalMouseListener;
import fr.faseldi.controllers.ProjectRunController;
import fr.faseldi.vues.OutputVue;
import fr.faseldi.vues.PreferencesVue;

/**
 *
 * @author faseldi
 */
public class FactorySingleton {
    public static GlobalMouseListener getMouseListener(){
        return GlobalMouseListener.getInstance();
    }
    public static GlobalKeyListener getKeyListener(){
        return GlobalKeyListener.getInstance();
    }
    public static OutputVue getOutputVue(){
        return OutputVue.getInstance();
    }
    public static ProjectRunController getProjectRun(){
        return ProjectRunController.getInstance();
    }
    public static PreferencesVue getPreferenceVue(){
        return PreferencesVue.getInstance();
    }
}
