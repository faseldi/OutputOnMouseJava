package fr.faseldi.outputonmousejava;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
/**
 *
 * @author faseldi
 */
public class OutputOnMouseJava {
    /**
     * remove listeners when the jvm is closed
     * important to avoid jnativehook bugs
     */
    public static void initOnExit(){
        Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new Thread( () -> {
            GlobalScreen.removeNativeKeyListener(FactorySingleton.getKeyListener());
            GlobalScreen.removeNativeMouseMotionListener(FactorySingleton.getMouseListener());
        }));
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        try{
            GlobalScreen.registerNativeHook();
        }catch(NativeHookException e){
            System.err.println(" Erreur au chargement du keylistener global");
            System.exit(1);
        }
        initOnExit();
        GlobalScreen.addNativeKeyListener(FactorySingleton.getKeyListener());
        GlobalScreen.addNativeMouseMotionListener(FactorySingleton.getMouseListener());
    }
    
}
